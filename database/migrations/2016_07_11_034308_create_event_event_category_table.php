<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventEventCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_event_category', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('event_category_id')->unsigned()->index();
            $table->integer('event_id')->unsigned()->index();

            $table->primary(['event_id', 'event_category_id']);

            $table->foreign('event_id')->references('id')->on('event')->onDelete('cascade');
            $table->foreign('event_category_id')->references('id')->on('event_category')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('event_event_category');
    }
}
