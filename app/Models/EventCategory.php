<?php namespace App\Models;

//use App\Models\Relations\BelongsToCustomerTrait;

class EventCategory extends \Baum\Node
{
    use TimestampsFormatTrait;


    protected $table = 'event_category';

    public $timestamps = true;
    protected $hidden = ['lft', 'rgt'];

    protected $with = [];

    public function events()
    {
        return $this->belongsToMany('App\Models\Event', 'event_event_category');
    }

}
