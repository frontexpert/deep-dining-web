<?php namespace App\Models;

//use App\Models\Relations\BelongsToCustomerTrait;
use Carbon;
use App\Models\Language;

class Event extends BaseModel
{
    use TimestampsFormatTrait;
    use \Conner\Tagging\TaggableTrait;
    use PublishedAtTimestampsFormatTrait;


    protected $table = 'event';
    public $timestamps = true;

    public function categories()
    {
        return $this->belongsToMany('App\Models\EventCategory', 'event_event_category');
    }

}
