<?php namespace App\Http\Controllers\Api;


use Input;
use Validator;

use App\Models\Event;
use App\Transformers\EventTransformer;

use App\Exceptions\NotFoundException;
use App\Exceptions\ResourceException;

class EventController extends ApiController
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $validator = Validator::make(Input::all(), [
            'ids'              => 'array|integerInArray',
            'page'             => 'integer',
            'published_at_min' => 'date_format:"Y-m-d H:i:s"',
            'published_at_max' => 'date_format:"Y-m-d H:i:s"',
            'created_at_min'   => 'date_format:"Y-m-d H:i:s"',
            'created_at_max'   => 'date_format:"Y-m-d H:i:s"',
            'updated_at_min'   => 'date_format:"Y-m-d H:i:s"',
            'updated_at_max'   => 'date_format:"Y-m-d H:i:s"',
            'limit'            => 'integer|min:1|max:250',
            'category_ids'     => 'array|integerInArray',
        ]);
        if ($validator->fails()) {
            throw new ResourceException($validator->errors()->first());
        }

        $events = new Event;
        $events = $events->with([
            'tagged',
            'categories' => function ($query) {
                $query->select(['event_category_id']);
            },
        ])->orderBy('published_at', 'DESC');

        //Filter
        if (Input::has('search')) {
            $events = $events->where('title', 'LIKE', '%' . Input::get('search') . '%');
        }

        if (Input::has('ids')) {
            $events = $events->whereIn('id', Input::get('ids'));
        }

        if (Input::has('category_ids')) {
            $events = $events->whereHas('categories', function ($q) {
                $q->whereIn('id', Input::get('category_ids'));
            });
        }
        if (Input::has('published_at_min')) {
            $events = $events->where('published_at', '>=', Input::get('published_at_min'));
        }
        if (Input::has('published_at_max')) {
            $events = $events->where('published_at', '<=', Input::get('published_at_max'));
        }

        if (Input::has('created_at_min')) {
            $events = $events->where('created_at', '>=', Input::get('created_at_min'));
        }
        if (Input::has('created_at_max')) {
            $events = $events->where('created_at', '<=', Input::get('created_at_max'));
        }
        if (Input::has('updated_at_min')) {
            $events = $events->where('updated_at', '>=', Input::get('updated_at_min'));
        }
        if (Input::has('updated_at_max')) {
            $events = $events->where('updated_at', '<=', Input::get('updated_at_max'));
        }

        $events = $events->simplePaginate(Input::get('limit', 50));

        return response()->paginator($events, new EventTransformer);

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $event = Event::find($id);

        $this->checkExist($event);

        $event = $event->load([
            'tagged',
            'categories' => function ($query) {
                $query->select(['event_category_id']);
            },
        ]);

        return response()->item($event, new EventTransformer);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $rules = [
            'published_at' => 'date_format:"Y-m-d H:i:s"',
            'categories'   => 'array|not_inInArray:1|integerInArray|existsInArray:event_category,id',
            'status'       => 'in:published,draft,trash',
            'visibility'   => 'in:public,private',
            'tags'         => 'array|stringInAarray',
            'title'        => 'min:1|max:100',
            'slug'         => 'required|alpha_dash|min:1|max:100',
            'content'      => '',
        ];

        $validator = Validator::make(Input::only(array_keys($rules)), $rules);

        if ($validator->fails()) {
            throw new ResourceException($validator->errors()->first());
        }
        $event = new Event;

        $this->fillFieldFromInput($event, ['slug', 'status', 'visibility', 'published_at']);
        $this->fillNullableFieldFromInput($event, ['title', 'content']);

        $event->save();

        $event->categories()->sync(Input::get('categories', []));
        if (Input::has('tags')) {
            $event->retag(Input::get('tags'));
        }

        return $this->show($event->id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update($id)
    {
        $rules = [
            'published_at' => 'date_format:"Y-m-d H:i:s"',
            'status'       => 'in:published,draft,trash',
            'visibility'   => 'in:public,private',
            'categories'   => 'array|not_inInArray:1|integerInArray|existsInArray:event_category,id',
            'tags'         => 'array|stringInAarray',
            'title'        => 'min:1|max:100',
            'slug'         => 'alpha_dash|min:1|max:100',
            'content'      => '',
        ];

        $validator = Validator::make(Input::only(array_keys($rules)), $rules);
        if ($validator->fails()) {
            throw new ResourceException($validator->errors()->first());
        }

        $event = Event::find($id);
        $this->checkExist($event);
        $this->fillFieldFromInput($event, ['slug', 'status', 'visibility', 'published_at']);
        $this->fillNullableFieldFromInput($event, ['title', 'content']);


        $event->save();

        $event->categories()->sync(Input::get('categories', []));
        if (Input::has('tags')) {
            $event->retag(Input::get('tags'));
        }

        return $this->show($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $event = Event::find($id);
        $this->checkExist($event);

        $event->delete();

        return response()->return();

    }


}
